﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //5.:  Crear una aplicacion que muestre la cantidad de 0 que hay del 1 al 100.

            //int num = 1;
            int a = 0;
            for (int num=1; num <=100; num++)
            {
                int res = num % 10;
                if (res==0)
                {
                    a++;
                }
            }
            Console.WriteLine("la cantidad de ceros entre 1 y 100 son " + (a+1));

            Console.ReadKey();
        }
    }
}
