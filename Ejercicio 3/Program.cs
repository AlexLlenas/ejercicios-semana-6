﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.:rear un programa que me diga la cantidad de digitos que tiene un numero positivo. 
            //Mostrar en mensaje ej: El numero ingresado tiene (N) digitos.

            Console.WriteLine("Inserte un número entero: ");
            long a = Convert.ToInt64(Console.ReadLine());
            
            //Console.WriteLine(a.ToString().Count());

            Console.WriteLine("Su número tiene {0} dígitos", (a.ToString().Count()));

            Console.ReadKey();
        }
    }
}
