﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.: Crear una aplicacion que muestre la tabla de multiplicar del (7), tips usar ciclo while.

            Console.WriteLine("Diga cuantas veces quieres multiplicar por 7: ");
            int a = Convert.ToInt32(Console.ReadLine());
            //int b = Convert.ToInt32(Console.ReadLine());
            int c = 0;
            while (c<=a)
            {
                int d = (c * 7);
                Console.WriteLine("{0} x 7 = {1}", c, d);
                c = c + 1;
            }

            Console.ReadKey();
        }
    }
}
