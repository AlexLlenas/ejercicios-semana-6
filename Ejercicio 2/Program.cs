﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.: Crear una aplicacion que le pida al usuario un numero positivo y el sistema identifique si es primo o no.

            Console.WriteLine("Introduce un número:");
            int a = Convert.ToInt32(Console.ReadLine());
            int c = 0;

            for (int b = 1; b < a + 1; b++)
            {
                if (a % b == 0)
                {
                    c++;
                }
            }
            if (c>2)
            {
                Console.WriteLine("El número no es primo");
            }
            else
            {
                Console.WriteLine("El número es primo");

            }

            Console.ReadKey();
        }
    }
}
